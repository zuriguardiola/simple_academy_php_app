# Simple PHP web application

This application is a simple web application to be deployed where a web app is required with a simple MySQL DB on the backend.

# Installation

You will need a MySQL database server with a user that is allowed to access the database from a remote server.

The user name should be: academy

The password should be made up by you and my application needs to be able to see that password from a file called /var/secrets/password, which should be read only by root and the web server user.